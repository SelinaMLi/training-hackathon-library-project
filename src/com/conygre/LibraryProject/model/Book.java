package com.conygre.LibraryProject.model;

public class Book extends LibraryObject{

    private Genre genre;
    private String author;
    private int publicationYear;

    public Book(String title,int id,Genre genre,String author,int publicationYear) {
        super(title,id);
        this.genre = genre;
        this.author = author;
        this.publicationYear = publicationYear;
    }

    public String toString(){
        return "Book: "+this.getTitle()+","+this.author+","+this.genre+","+this.publicationYear;
    }

    @Override
    public boolean canBorrow() {
        return true;
    }
}
