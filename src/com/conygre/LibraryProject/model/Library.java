package com.conygre.LibraryProject.model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Library {

    private ArrayList<LibraryObject> libraryInventory;
    private ArrayList<Transaction>  transactions;

    public Library(){
        this.libraryInventory = new ArrayList<LibraryObject>();
        transactions = new ArrayList<Transaction>();
    }

    public void addToInventory(LibraryObject o){
        this.libraryInventory.add(o);
    }

    public void removeFromInventory(LibraryObject o){
        this.libraryInventory.remove(o);
    }

    public void removeFromInventory(int index){
        this.libraryInventory.remove(index);
    }

    public Transaction borrowItem(LibraryObject o,int userId){

        //if inventory contains target item, add to transactions list.
        if(libraryInventory.contains(o)){
            Transaction t =new Transaction(
                    o.getId(),
                    userId,
                    LocalDate.now(),
                    LocalDate.now().plus(1, ChronoUnit.WEEKS)
            );
            transactions.add(t);//due date is 1 week from now
            libraryInventory.remove(o);
            return t;
        }
        else{
            return null;
        }
    }

    public Transaction borrowItem(int index,int userId){

       if(index<libraryInventory.size()){
           Transaction t = new Transaction(
                   libraryInventory.get(index).getId(),
                   userId,
                   LocalDate.now(),
                   LocalDate.now().plus(1, ChronoUnit.WEEKS)
           );
           transactions.add(t);
           libraryInventory.remove(index);
           return t;
       }
       else return null;
    }

    public void returnItem(LibraryObject o,int userId){

        for(Transaction t:transactions){
            if(t.getItemId()==o.getId() && t.getUserId()==userId){
                transactions.remove(t);
                libraryInventory.add(o);
                break;
            }
        }
    }

    public String printInventoryList(){
        StringBuilder list = new StringBuilder();

        for(int i=0;i<libraryInventory.size();i++){
            //System.out.println(libraryInventory.get(i).toString());
            list.append(i+"  "+libraryInventory.get(i).toString()+"\n");
        }

        return list.toString();
    }

    public ArrayList<LibraryObject> getLibraryInventory() {
        return libraryInventory;
    }
}
