package com.conygre.LibraryProject.model;

public class DVD extends LibraryObject {

    private Genre genre;
    private String director;
    private int releaseYear;

    public DVD(String title,int id,Genre genre, String director, int releaseYear) {
        super(title,id);
        this.genre = genre;
        this.director = director;
        this.releaseYear = releaseYear;
    }

    public String toString(){
        return "DVD: "+getTitle()+","+this.director+","+this.genre+","+this.releaseYear;
    }

    @Override
    public boolean canBorrow() {
        return false;
    }
}
