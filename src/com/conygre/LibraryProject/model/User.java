package com.conygre.LibraryProject.model;

public class User {

    private String name;
    private String userId;

    public User(String name, String userId) {
        this.name = name;
        this.userId = userId;
    }
}
