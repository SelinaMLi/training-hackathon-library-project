package com.conygre.LibraryProject.model;

import java.time.LocalDate;

public class Transaction {

    private int itemId;
    private int userId;
    private LocalDate borrowedDate;
    private LocalDate dueDate;

    public Transaction(int itemId, int userId, LocalDate borrowedDate, LocalDate dueDate) {
        this.itemId = itemId;
        this.userId = userId;
        this.borrowedDate = borrowedDate;
        this.dueDate = dueDate;
    }

    public int getItemId() {
        return itemId;
    }

    public int getUserId() {
        return userId;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }
}
