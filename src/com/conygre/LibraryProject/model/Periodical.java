package com.conygre.LibraryProject.model;

public class Periodical extends LibraryObject{

    public Periodical(String title,int id) {
        super(title,id);
    }

    @Override
    public boolean canBorrow() {
        return false;
    }

    public String toString(){
        return "Periodical: "+this.getTitle();
    }
}
