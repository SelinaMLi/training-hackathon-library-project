package com.conygre.LibraryProject.model;

public class CD extends LibraryObject{

    private MusicGenre genre;
    private String artist;

    public CD(String title,int id,MusicGenre genre, String artist) {
        super(title,id);
        this.genre = genre;
        this.artist = artist;
    }

    public String toString(){
        return "CD: "+this.getTitle()+","+this.artist+","+this.genre;
    }

    @Override
    public boolean canBorrow() {
        return true;
    }
}
