package com.conygre.LibraryProject.model;

public enum Genre {
    Fiction,
    NonFiction,
    Comedy,
    Romance,
    SciFi,
    Horror,
    Gore,
    Thriller,
    Action,
    Adventure,
    Music
}
