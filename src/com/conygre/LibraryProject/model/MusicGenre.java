package com.conygre.LibraryProject.model;

public enum MusicGenre {
    Rock,
    RNB,
    Soul,
    Funk,
    Electronic,
    Country,
    Pop
}
