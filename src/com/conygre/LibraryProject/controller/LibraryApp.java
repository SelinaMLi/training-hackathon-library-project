package com.conygre.LibraryProject.controller;

import com.conygre.LibraryProject.model.*;

import java.util.Scanner;

public class LibraryApp {

    public static void main(String[] args){

        Library library = new Library();

        library.addToInventory(new Book("To Kill A Mockingbird",1,Genre.Fiction,"Harper Lee",1960));
        library.addToInventory(new Book("Tale of Two Cities",2,Genre.Fiction,"Charles Dickens",1859));
        library.addToInventory(new DVD("Kill Bill",3, Genre.Action, "Quentin Tarantino", 2003));
        library.addToInventory(new DVD("Coco",4,Genre.Adventure,"Adrian Molina",2017));
        library.addToInventory(new CD("Abbey Road",5,MusicGenre.Pop,"The Beatles"));
        library.addToInventory(new Periodical("Time",6));

        Scanner scan = new Scanner(System.in);
        String actionPrompt = "Enter your action: v-view library list, a-add to list, d-delete from inventory, b-borrow, x-exit";
        System.out.println(actionPrompt);
        String userInput = scan.nextLine();

        while(!userInput.equals("x")){

            switch(userInput){
                case "v":
                    System.out.println(library.printInventoryList());
                    break;
                case "a":
                    library.addToInventory(createObjectFromUser());
                case "d":
                    System.out.print("Enter item index to remove:");
                    int index = scan.nextInt();
                    library.removeFromInventory(index);
                    break;
                case "b":
                    System.out.print("Enter item index to borrow:");
                    index = scan.nextInt();
                    Transaction t = library.borrowItem(index,0);
                    if(t!=null){
                        System.out.println("Item borrowed, due date is "+t.getDueDate().toString());
                    }
                    else{
                        System.out.println("Error borrowing item");
                    }
                    break;
                default:
                    break;
            }

            System.out.println(actionPrompt);
            userInput = scan.nextLine();
        }
    }

    public static LibraryObject createObjectFromUser(){

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter item with following format:");
        String input = scan.nextLine();

        switch(input){
            case "book":
                System.out.println("Enter genre:");


        }

        return null;
    }
}
